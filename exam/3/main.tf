provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "nginx" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.first_security_group.id]
  key_name = "anastasiarzevskaa@MacBook"
  user_data = <<-USERDATA
    #cloud-config
    package_upgrade: true
    packages:
      - nginx
    runcmd:
      - systemctl enable nginx
      - systemctl start nginx
      - sudo mkdir /var/www/html/test1
      - sudo mkdir /var/www/html/test2
      - echo 'test1' | sudo tee /var/www/html/test1/file1.txt
      - echo 'test2' | sudo tee /var/www/html/test2/file2.txt
      - sudo chown -R nginx:nginx /var/www/html
    USERDATA
    }   

resource "aws_security_group" "first_security_group" {
  name        = "first_security_group"
  description = "Allow SSH and HTTP inbound traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "public_ip" {
  value = aws_instance.nginx.public_ip
}

