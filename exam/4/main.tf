provider "aws" {
  region = "eu-central-1"
}
data "aws_ami" "ubuntu" {
  owners = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
data "template_file" "user_data" {
  template = file("01-ssh-public-key-config.tpl")
  vars = {
    key_owner = "anastasiarzevskaa@MacBook-Pro-Anastasia-Rzevskaa.local"
  }
}
resource "aws_instance" "log" {
    ami         = data.aws_ami.ubuntu.id
    instance_type = "t2.micro"
    key_name   = "anastasiarzevskaa@MacBook"
    vpc_security_group_ids = [aws_security_group.log_security_group.id]

  tags = {
    Name = "log"
  }
    connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("~/.ssh/id_rsa")
    host        = self.public_ip
  }
  provisioner "remote-exec" {
    inline = [
        "sudo apt-get update",
        "sudo apt-get install -y tar",
        "sudo mkdir -p /var/log/test-log-old",
        "sudo cp ./log_script.sh tmp/log_script.sh",
        "sudo bash -c 'chmod +x tmp/log_script.sh'",
        "sudo bash -c 'tmp/log_script.sh'"
    ]
  }
}
resource "aws_security_group" "log_security_group" {
  name        = "log-security-group"
  description = "Security group"
  dynamic "ingress" {
    for_each = var.ingress_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
variable "ingress_ports" {
  type        = list(any)
  description = "allowed incoming ports"
  default     = ["22"]
}