#!/bin/bash

log_dir="/var/log/test-log"
archive_dir="/var/log/test-log-old"
log_limit=1000000  # Розмір логів в байтах, при якому вони будуть архівовані

# Створення папки для логів, якщо вона ще не існує
mkdir -p "$log_dir"

# Додавання тестових логів
echo "Test log message 1" >> "$log_dir/test-log-1.log"
echo "Test log message 2" >> "$log_dir/test-log-2.log"
echo "Test log message 3" >> "$log_dir/test-log-3.log"

# Перевірка розміру логів
log_size=$(du -b "$log_dir" | awk '{print $1}')

if [[ $log_size -gt $log_limit ]]; then
  # Архівація логів
  timestamp=$(date +%Y%m%d%H%M%S)
  archive_file="$archive_dir/logs_$timestamp.tar.gz"
  
  tar -czvf "$archive_file" "$log_dir"/*
  
  # Видалення старих логів
  rm -rf "$log_dir"/*
fi
