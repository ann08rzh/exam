#!/bin/bash
sudo apt-get update
sudo apt-get install -y postgresql
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/12/main/postgresql.conf
echo "host all all ${aws_instance.virtual_server_2.private_ip}/32 md5" | sudo tee -a /etc/postgresql/12/main/pg_hba.conf
sudo systemctl restart postgresql

DB_HOST="localhost"
DB_PORT="5432"
DB_NAME="mydatabase"
DB_USER="myuser"
DB_PASSWORD= var.db_password
SQL_QUERY="INSERT INTO your_table (column1, column2, column3) VALUES ('value1', 'value2', 'value3');"
PGPASSWORD="${DB_PASSWORD}" psql -h "${DB_HOST}" -p "${DB_PORT}" -U "${DB_USER}" -d "${DB_NAME}" -c "${SQL_QUERY}"
