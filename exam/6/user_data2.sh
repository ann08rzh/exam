#!/bin/bash
mkdir -p /home/ubuntu/postgres-dumps
find /home/ubuntu/postgres-dumps/* -mtime +5 -exec rm {} \;
PGPASSWORD="${DB_PASSWORD}" pg_dump -h ${aws_instance.virtual_server_1.public_ip} -U postgres -d mydatabase -f /home/ubuntu/postgres-dumps/backup_$(date +%Y%m%d_%H%M%S).sql