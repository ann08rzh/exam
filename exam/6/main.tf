provider "aws" {
  region = var.region
}
resource "aws_instance" "virtual_server_1" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.postgress_security_group.id]
  key_name = "anastasiarzevskaa@MacBook"
  tags = {
    Name       = "virtual_server_1"
    Env        = "production"
    ServerType = "postgresql"
  }
  user_data = templatefile("01-ssh-public-key-config.tpl", {key_owner = "anastasiarzevskaa@MacBook-Pro-Anastasia-Rzevskaa.local"}) 
  
}
resource "aws_instance" "virtual_server_2" {
  ami           = data.aws_ami.ubuntu.id 
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.postgress_security_group.id]
  key_name = "anastasiarzevskaa@MacBook"
  tags = {
    Name       = "virtual_server_2"
    Env        = "production"
    ServerType = "pg_dump"
  }
  user_data = templatefile("01-ssh-public-key-config.tpl", {key_owner = "anastasiarzevskaa@MacBook-Pro-Anastasia-Rzevskaa.local"})
  
}
resource "aws_security_group" "postgress_security_group" {
  name        = "postgress-security-group"
  description = "Security group for Postgres access"
  dynamic "ingress" {
    for_each = var.ingress_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "null_resource" "copy_and_run_scripts" {
  depends_on = [
    aws_instance.virtual_server_1,
    aws_instance.virtual_server_2
  ]
  provisioner "file" {
    source      = "./user_data1.sh"  
    destination = "/tmp/user_data1.sh"
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
      host        = aws_instance.virtual_server_1.public_ip
    }
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/user_data1.sh", 
      "echo 'Running user_data1.sh script'",
      "bash /tmp/user_data1.sh" 
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
      host        = aws_instance.virtual_server_1.public_ip
    }
  }
  provisioner "file" {
    source      = "./user_data2.sh"
    destination = "/tmp/user_data2.sh"
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
      host        = aws_instance.virtual_server_2.public_ip
    }
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/user_data2.sh",
      "bash /tmp/user_data2.sh" 
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
      host        = aws_instance.virtual_server_2.public_ip
    }
  }
}

