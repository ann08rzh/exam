output "instance_ids" {
  value = {
    virtual_server_1_id = aws_instance.virtual_server_1.id
    virtual_server_2_id = aws_instance.virtual_server_2.id
  }
}
output "public_ips" {
  value = {
    virtual_server_1_public_ip = aws_instance.virtual_server_1.public_ip
    virtual_server_2_public_ip = aws_instance.virtual_server_2.public_ip
  }
}
output "aws_region" {
  value = data.aws_region.current.name
}
output "aws_availability_zones" {
  value = data.aws_availability_zones.available_zones.names
}
