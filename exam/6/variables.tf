variable "region" {
  description = "aws region"
  default     = "eu-central-1"
}
variable "instance_type" {
  description = "instance type"
  default     = "t2.micro"
}
variable "db_password" {
  description = "Database password"
  type        = string
}
variable "ingress_ports" {
  type        = list(any)
  description = "allowed incoming ports"
  default     = ["5432", "22"]
}