variable "region" {
  description = "aws region"
  default     = "eu-central-1"
}
variable "instance_type" {
  description = "instance type"
  default     = "t2.micro"
}
variable "db_password" {
  description = "Database password"
  type        = string
}
variable "ingress_ports" {
  type        = list(any)
  description = "allowed incoming ports"
  default     = ["5432", "22"]
}
variable ssh_user {
  description = "User account for ssh access to the image"
  default     = "opc"
}

variable ssh_private_key {
  description = "File location of the ssh private key"
  default     = "./id_rsa"
}

variable ssh_public_key {
  description = "File location of the ssh public key"
  default     = "./id_rsa.pub"
}