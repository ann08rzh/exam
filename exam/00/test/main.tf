data "aws_region" "current" {}

data "aws_availability_zones" "available_zones" {
  state = "available"
}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_instances" "instance" {
  instance_tags = {
    Role = "server"
  }

  filter {
    name   = "instance.group-id"
    values = [aws_security_group.sg.id]
  }

  instance_state_names = ["pending", "running", "shutting-down", "terminated", "stopping", "stopped"]
}

resource "aws_instance" "virtual_server" {
  ami             = data.aws_ami.ubuntu.id
  instance_type   = var.instance_type
  count           = var.instance_count
  key_name        = "anastasiarzevskaa@MacBook"
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.sg.id]
  user_data       = data.template_file.user_data.rendered
  tags = {
    Name = var.instance_names[count.index]
  }
}

resource "aws_security_group" "sg" {
  name        = "sg"
  description = "Security Group"

  dynamic "ingress" {
    for_each = var.ingress_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "template_file" "user_data" {
  template = file("${path.module}/01-ssh-public-key-config.tpl")
  vars = {
    key_owner = "anastasiarzevskaa@MacBook-Pro-Anastasia-Rzevskaa.local"
  }
}

resource "aws_eip" "test" {
  count    = var.instance_count
  instance = aws_instance.virtual_server[count.index].id
}

resource "aws_ebs_volume" "volume" {
  count             = var.instance_count
  availability_zone = data.aws_availability_zones.available_zones.names[0]

  size = var.volume_size
}

resource "aws_volume_attachment" "volume" {
  count       = var.instance_count
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.volume[count.index].id
  instance_id = aws_instance.virtual_server[count.index].id
}

output "volume_ids" {
  value = aws_ebs_volume.volume[*].id
}
