provider "aws" {
  region     = var.region  
}
resource "aws_instance" "vm1" {
  ami           = data.aws_ami.ubuntu.id     
  instance_type = "t2.micro"  
  key_name      = "anastasiarzevskaa@MacBook"
  vpc_security_group_ids = [aws_security_group.postgresql_sg.id, aws_security_group.ssh_sg.id]
  tags = {
    Name = "VM1"
  }
}
resource "aws_instance" "vm2" {
  ami           = data.aws_ami.ubuntu.id     
  instance_type = "t2.micro"  
  key_name      = "anastasiarzevskaa@MacBook"
  vpc_security_group_ids = [aws_security_group.ssh_sg.id]
  tags = {
    Name = "VM2"
  }
}
resource "tls_private_key" "generated_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  public_key = tls_private_key.generated_key.public_key_openssh
}

resource "aws_security_group" "postgresql_sg" {
  name        = "postgresql_sg"
  description = "Allow PostgreSQL inbound traffic"

  ingress {
    from_port   = 5432 
    to_port     = 5432  
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] 
  }
}
resource "aws_eip" "private_ip_vm1" {
  domain   = "vpc"
  instance = aws_instance.vm1.id
}
resource "aws_security_group" "ssh_sg" {
  name        = "ssh_sg"
  description = "Allow SSH inbound traffic"

  ingress {
    from_port   = 22  
    to_port     = 22  
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] 
  }
}

resource "aws_security_group_rule" "postgresql_access" {
  type              = "ingress"
  from_port         = 5432
  to_port           = 5432
  protocol          = "tcp"
  security_group_id = aws_security_group.postgresql_sg.id
  source_security_group_id = aws_security_group.ssh_sg.id  # Предполагая, что vm2 уже имеет отдельную группу без доступа по умолчанию
}

resource "null_resource" "test_connection" {
  depends_on = [
    aws_security_group_rule.postgresql_access
  ]

  provisioner "remote-exec" {
    connection {
    type        = "ssh"
    host        = "${var.public_ip}"
    private_key = "${file(var.ssh_private_key)}"
    user        = "${var.ssh_user}"
    timeout     = "5m"
  }
    inline = [
      "ping -c 3 ${aws_eip.private_ip_vm1.public_ip}",
      "psql -h ${aws_eip.private_ip_vm1.public_ip} -U YOUR_POSTGRES_USER -d YOUR_DATABASE -c 'SELECT 1;'"
      # Замініть YOUR_POSTGRES_USER і YOUR_DATABASE на відповідні значення
    ]
  }
}