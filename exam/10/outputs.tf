output "instance_ids" {
  value = {
    virtual_server_id = aws_instance.virtual_server.id
    
  }
}
output "public_ips" {
  value = {
    virtual_server_public_ip = aws_instance.virtual_server.public_ip 
  }
}
output "aws_region" {
  value = data.aws_region.current.name
}
output "aws_availability_zones" {
  value = data.aws_availability_zones.available_zones.names[0]
}
