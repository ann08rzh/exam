variable "region" {
  description = "aws region"
  default     = "eu-central-1"
}
variable "instance_type" {
  description = "instance type"
  default     = "t2.micro"
}
variable "aws_instance" {
  description = "Конфігурація віртуальних машин"
  default     = []
}
variable "instance_names" {
  type    = list(string)
  default = ["instance-1", "instance-2", "instance-3"]
}
variable "ingress_ports" {
  type        = list(any)
  description = "allowed incoming ports"
  default     = ["5432", "22","3000", "22", "80", "3001", "3002", "3003", "3004", "8080", "9090", "443"]
}
variable "instance_count" {
  description = "Кількість віртуальних машин"
  default     = 3
  type    = number
 
}
variable "volume_size" {
  description = "Розмір вольюму (гігабайти)"
  default     = 20
}