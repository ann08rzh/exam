provider "aws" {
  region = var.region
}
resource "aws_instance" "virtual_server" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  count         = var.instance_count
  name          = var.instance_names[count.index]
  key_name      = "anastasiarzevskaa@MacBook"
  tags = {
    Name = var.instance_names[count.index]
}

  user_data = templatefile("01-ssh-public-key-config.tpl", { key_owner = "anastasiarzevskaa@MacBook-Pro-Anastasia-Rzevskaa.local" })

  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.sg.id]
}

resource "aws_eip" "eip" {
  count = var.instance_count
}

resource "aws_security_group" "sg" {
  name        = "sg"
  description = "Security Group"

  dynamic "ingress" {
    for_each = var.ingress_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_ebs_volume" "volume" {
  count             = var.instance_count
  availability_zone = data.aws_availability_zones.available_zones[0]

  size = var.volume_size
}

resource "aws_volume_attachment" "volume" {
  count       = var.instance_count
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.volume[count.index].id
  instance_id = aws_instance.virtual_server[count.index].id
}

output "volume_ids" {
  value = aws_ebs_volume.volume[*].id
}
