data "aws_region" "current" {}

data "aws_availability_zones" "available_zones" {
  state = "available"
}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_instances" "instance" {
  instance_tags = {
    Role = "server"
  }

  filter {
    name   = "instance.group-id"
    values = [aws_security_group.sg.id]
  }

}

resource "aws_eip" "test" {
  count    = length(data.aws_instances.instance.ids)
  instance = data.aws_instances.instance.ids[count.index]
}

data "template_file" "user_data" {
  template = file("01-ssh-public-key-config.tpl")
  vars     = {
    key_owner = "anastasiarzevskaa@MacBook-Pro-Anastasia-Rzevskaa.local"
  }
}
